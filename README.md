# barAPI Open API project

Needed software: NodeJS, MongoDB (,git)


# To Start Developing/Testing:

> *git clone (this repo)*

> *cd (repo)*

> *npm install*

> *npm start*


# API Calls (Default local port: 4200)


## Requesting all of database

> */API/bars*

##### **This returns JSON like:**
```javascript
{
  "_id":"59de02846d870c3200b452b5",
  "name":"placeholder",
  "locx":"60.192059",
  "locy":"24.945831",
  "products" {:
    "Kalja1": {
      "name":"asd",
      "price":4.5
    },
    "Kalja2":{
      "name":"sdf",
      "price":3
    },
    "Salmari":{
      "name":"qwe",
      "price":2}},
      "type":"pub",
      "capacity":"60"
    }
,{
  "_id":"59de04b6396de52bf44bba9d",
  "name":"'esim'",
  "locx":"20.1234",
  "locy":"20.1234",
  "products":null,
  "type":"'bar'",
  "capacity":"100"
}
,{
  "_id":"59de0620396de52bf44bba9e",
  "name":"Testi2",
  "locx":"60.1234",
  "locy":"21.1234",
  "products":null,
  "type":"pub",
  "capacity":"200"
}
,{
  "_id":"59de0677396de52bf44bba9f",
  "name":"esim",
  "locx":"20.1234",
  "locy":"20.1234",
  "products":null,
  "type":"bar","capacity":"100"
}
```


## **Searching content with query**

> */API/bars?name=placeholder*


This returns JSON like:
```javascript
{
  "_id":"59de02846d870c3200b452b5",
  "name":"placeholder",
  "locx":"60.192059",
  "locy":"24.945831",
  "products" {:
    "Kalja1": {
      "name":"asd",
      "price":4.5
    },
    "Kalja2":{
      "name":"sdf",
      "price":3
    },
    "Salmari":{
      "name":"qwe",
      "price":2}},
      "type":"pub",
      "capacity":"60"
    }
```


## **Finding nearby bars by either address or coordinates**

> */API/bars?address='Bulevardi 31'*

This returns JSON like:

```javascript
{
  "_id":"59de02846d870c3200b452b5",
  "name":"placeholder",
  "locx":"60.192059",
  "locy":"24.945831",
  "products" {:
    "Kalja1": {
      "name":"asd",
      "price":4.5
    },
    "Kalja2":{
      "name":"sdf",
      "price":3
    },
    "Salmari":{
      "name":"qwe",
      "price":2}},
      "type":"pub",
      "capacity":"60"
    }
,{
  "_id":"59de04b6396de52bf44bba9d",
  "name":"'esim'",
  "locx":"20.1234",
  "locy":"20.1234",
  "products":null,
  "type":"'bar'",
  "capacity":"100"
}
,{
  "_id":"59de0620396de52bf44bba9e",
  "name":"Testi2",
  "locx":"60.1234",
  "locy":"21.1234",
  "products":null,
  "type":"pub",
  "capacity":"200"
}
,{
  "_id":"59de0677396de52bf44bba9f",
  "name":"esim",
  "locx":"20.1234",
  "locy":"20.1234",
  "products":null,
  "type":"bar","capacity":"100"
}
```


## **Adding a object with AJAX query (Needs API Key from Dev panel)**

> */API/addobject?name=esim&locx=20.1234&locy=20.1234&type=bar*
> *&capacity=100&API_key=asd*

This inserts the bar object and returns to index page



## **Adding a object to database**

> */API/addobject*

This request needs a form with inputs (And you to be logged in):

**name="barname", type="input"**

**name="locx", type="number", step="0.0001")**

**name="locy", type="number", step="0.0001"**

**name="type", type="text"**

**name="capacity", "type=number"**


This inserts the bar object and returns to index page


API Front End can be found in localhost:4200

![BAR Api screenshot](./screenshots/1.png)
