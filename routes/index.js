var express = require('express');
var router = express.Router();
var mongodb = require('mongodb');
var MongoClient = require('mongodb').MongoClient, test = require('assert');
var ObjectID = require('mongodb').ObjectID;
var monk = require('monk');
var db = monk('localhost:27017/nodetest1');
var app = require('express')();

/*multer to manage images*/
var multer = require("multer");
var upload = multer({dest: "uploads/"});
var fs = require('fs');

var loggedin = app.get('loggedin');
var loggedinas="none";
var id;

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/dev', function(req, res, next) {
  var db = req.db;
  var collection = db.get('usercollection');
  collection.find({},{},function(e,docs){
    res.render('dev', {
      title: 'Express Developer Test',
      "loggedin": app.get('loggedin'),
      "userlist": docs
      });
  });
});
/* GET Demo #1 Router */
router.get('/demo', function(req, res, next) {
  res.render('demo', { title: 'Express BarAPI Demo' });
});
/* GET Demo #2 Router */
router.get('/demo2', function(req, res, next) {
  res.render('demo2', { title: 'Express BarAPI Demo2' });
});
/* GET Demo #3 Router */
router.get('/demo3', function(req, res, next) {
  res.render('demo3', { title: 'Express BarAPI Demo3' });
});
/* Login POST */
router.post('/login', function(req, res) {
  var db = req.db;
  var userEmail = req.body.loginEmail;
  var userPassword = req.body.loginPasswd;
  var collection = db.get('usercollection');
      collection.find({
          "email" : userEmail,
          "password": userPassword
      }).then(function () {
        app.set("loggedin", true);
        loggedinas = userEmail;
      });
  res.redirect('back');
});
/* Logout POST */
router.post('/logout', function(req, res) {
    app.set("loggedin", false);
    res.redirect('back');
});
/* Adduser POST with API_Key generation */
router.post('/adduser', function(req, res) {

  function generateUUID() {
    var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c){
      var r = (d + Math.random()*16)%16 | 0;
      d = Math.floor(d/16);
      return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;  }

    var db = req.db;
    var userEmail = req.body.signupEmail;
    var userPassword = req.body.signupPasswd;
    var collection = db.get('usercollection');
    var APIkey = generateUUID();
    var jatka=false
    do{
        jatka=false
        collection.find({
            "API_key" : APIkey
        }).then(function () {
          APIkey = generateUUID();
          jatka=true;
        });
    }
    while(jatka);
    collection.find({},{},function(e,docs){
        id=docs.length;
    });
    collection.insert({
        "email" : userEmail,
        "password": userPassword,
        "API_key": APIkey
    }, function (err, doc) {
        if (err) {
            res.send("There was a problem adding the information to the database.");
        }
        else {
            res.redirect('back');
        }
    });
});
router.post('/deluser', function(req, res) {
    var db = req.MongoClient;
    MongoClient.connect('mongodb://localhost:27017/nodetest1', function(err, db) {
              var collection = db.collection("usercollection");
              collection.removeOne({_id :ObjectID(req.body.delthisuser)}).then(function(r) {
                    db.close();
                  });
              });
    res.redirect("back");
});

module.exports = router;
