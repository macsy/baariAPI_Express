var express = require('express');
var router = express.Router();
var app = require('express')();

var mongo = require('mongodb');
var monk = require('monk');
var mongodb = require('mongodb');
var MongoClient = require('mongodb').MongoClient, test = require('assert');
var ObjectID = require('mongodb').ObjectID;

var https = require('https');
var http = require('http');

var loggedin =app.get('loggedin');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('a(href="../") Nothing here, go back');
});

/* GET bars */
router.get('/bars', function(req, res) {
	var db=req.db;
	var query = req.query;

	var collection = db.get("barcollection");

	if(req.query.alustus=="true"){// TESTAUKSEEN
		alustaDB(req, res);
	}

	else if(typeof req.query.locx!="undefined" && typeof req.query.locy!="undefined"){
		getclosest(req, res);
	}
  if(req.query.alustus == "true"){
    alustaDB(req, res);
  }
	else if(typeof req.query.address!="undefined"){
		getcoordinates(req, res);
	}
	else{
		collection.find(query).then((docs) => {
		res.send(docs);
		})
	}
});

/* GET 5 closest bars */
function getclosest(req, res){
	var db=req.db;
	var collection = db.get("barcollection");
	var xkoord = req.query.locx;
	var ykoord = req.query.locy;
	var hypotenuusat = [];
	var result = [];
	var a, b, c = null;
	var baarimäärä = 5; // Montako lähintä baaria palautetaan

	if(isNaN(req.query.amount)!=true){ // Rajoittaa baarien määrää jos "amount" annettu
		if(baarimäärä>req.query.amount){
			baarimäärä = +req.query.amount;
		}
	}

	collection.find({}).then((docs) => {
		for(var i=0; i<docs.length; i++){
			if(docs[i].locx > xkoord){
				aa = docs[i].locx - xkoord;
				a = Math.pow(aa, 2);
			}
			if(docs[i].locx <= xkoord){
				aa = xkoord - docs[i].locx;
				a = Math.pow(aa, 2);
			}
			if(docs[i].locy > ykoord){
				bb = docs[i].locy - ykoord;
				b = Math.pow(bb, 2);
			}
			if(docs[i].locy <= ykoord){
				bb=ykoord - docs[i].locy;
				b = Math.pow(bb, 2);
			}
			c = Math.sqrt(a+b);
			hypotenuusat.push({index:i, matka:c});
		}

		hypotenuusat.sort(järjestys);
		hypotenuusat.splice(baarimäärä);

		for(var j=0; j<hypotenuusat.length; j++){
			result.push(docs[hypotenuusat[j].index]);
		}
		res.send(result);
	})
}

// Järjestää lähimmät baarit välimatkan mukaan
function järjestys(a, b){
	if(a.matka < b.matka){
		return -1;
	}
	if(a.matka > b.matka){
		return 1;
	}
	return 0;
}

// Hakee koordinaatit osoitteen perusteella
function getcoordinates(req, res){
	var address = encodeURI(req.query.address);
	var googleUrl = "https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&key=AIzaSyDvge32cymE4fzmQzSNBJXVRHQcdQLJUcY";

	https.get(googleUrl, (resp) => {
		let data = '';

		// A chunk of data has been recieved.
		resp.on('data', (chunk) => {
			data += chunk;
		});

		// The whole response has been received, passes address coordinates to getclosest()
		resp.on('end', () => {
			var vastausJson = JSON.parse(data);
			req.query.locx = vastausJson.results[0].geometry.location.lat;
			req.query.locy = vastausJson.results[0].geometry.location.lng;
			getclosest(req, res);
		});

		}).on("error", (err) => {
		  console.log("Error: " + err.message);
		});
}

// Muokkaa csv tiedoston json muotoon ja tallettaa tarvittan datan databaseen
function alustaDB(req, res){
	var db=req.db;
    var collection = db.collection('barcollection');
	var vastausJson = [];

	//Muuttaa csv tiedoston jsoniksi
	const csvFilePath='./elintarvikehuoneistot.csv'
	const csv=require('csvtojson')
	csv({delimiter:","})
	.fromFile(csvFilePath)
	.on('json',(jsonObj)=>{
		if(jsonObj.type=="Pubitoiminta" || jsonObj.type=="Ravintolatoiminta"){
			vastausJson.push(jsonObj);
		}
	})
	.on('done',(error)=>{
		console.log(vastausJson.length);

		// Lisää datan databaseen
		for(var i=0; i<vastausJson.length; i++){
			var db=req.db;
			var collection = db.collection('barcollection');

			collection.insert({
				"name" : vastausJson[i].name,
				"locx" : vastausJson[i].locx,
				"locy": vastausJson[i].locy,
				"type" : vastausJson[i].type,
				"capacity" : vastausJson[i].capacity
			});
		}
		res.send("Database alustettu");
	})
}

function myLoop (i, vastausJson, koordArr, req) {
   setTimeout(function () {
      i++;
      if (i < vastausJson.length) {
		var address = encodeURI(vastausJson[i].address+" "+vastausJson[i].Postitmp);
		var googleUrl = "https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&key=AIzaSyDSBarLVsgx8A4iPCZaWT40KEQNpFecKLg";

		https.get(googleUrl, (resp) => {
			let data = '';
			var locx = null;
			var locy = null;

			// A chunk of data has been recieved.
			resp.on('data', (chunk) => {
				data += chunk;
			});

			// The whole response has been received, passes address coordinates to getclosest()
			resp.on('end', () => {
				var googleJson = JSON.parse(data);
				console.log(googleJson);
				if(googleJson.status=='OK'){
					locX = googleJson.results[0].geometry.location.lat;
					locY = googleJson.results[0].geometry.location.lng;
					console.log("Googlen antamat koordinaatit"+locX+" - "+locY);
					var koordObj = {locx:locX, locy:locY};
					console.log("Objektin koordinaatit"+koordObj.locx+" - "+koordObj.locy);
					koordArr.push(koordObj);
				}
			});

			}).on("error", (err) => {
			  console.log("Error: " + err.message);
			});

        myLoop(i, vastausJson, koordArr, req);
      }
	  else{
		  console.log("Elsessä"+vastausJson[0]);
		  console.log(typeof koordArr[0]);
		  alustusta(vastausJson, koordArr, req);}
   }, 25)
}

function alustusta(vastausJson, koordArr, req){
	console.log("Alustuksessa"+vastausJson[0]);
	console.log(koordArr[0]);
	for(var i=0; i<koordArr.length; i++){
		console.log("Databaseen asettaminen");
		console.log("googleKoordinaatit on: "+koordArr[0].locx);
		var db=req.db;
		var collection = db.collection('barcollection');

		collection.insert({
			"name" : vastausJson[i].name,
			"locx" : koordArr[i].locx,
			"locy": koordArr[i].locy,
			"type" : vastausJson[i].type,
			"capacity" : vastausJson[i].capacity
		});
	}
}

router.get('/addobject', function(req, res) {
    var keytrue=false;
    if(req.query.name!=""&&req.query.locx!=""&&req.query.locx!=""
    &&req.query.locy!=""&&req.query.type!=""
    &&req.query.capacity!=""&&req.query.API_key!=""){
    var name = req.query.name;
    var locx = req.query.locx;
    var locy = req.query.locy;
    var type = req.query.type;
    var capacity = req.query.capacity;
    var db=req.db;
    var collection = db.collection('usercollection');
    collection.find({
              "API_key" : req.query.API_key
          }).then(function () {
            keytrue=true;
          });
    }
    else{
    var name = req.body.barname;
    var locx = req.body.locx;
    var locy = req.body.locy;
    var type = req.body.type;
    var capacity = req.body.capacity;
    keytrue = app.get('loggedin');
  }

    /* polku + nimi tallennetaa */
    var db=req.db;
    var collection = db.collection('barcollection');
    collection.insert({
        "name" : name,
        "locx" : locx,
        "locy": locy,
        "products" : null,
        "type" : type,
        "capacity" : capacity
    }, function (err, doc) {
        if (err) {
            res.send("There was a problem adding the information to the database.");
        }
        else {
            res.redirect("back");
        }
    });
    });

module.exports = router;
