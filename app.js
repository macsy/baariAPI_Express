var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var mongo = require('mongodb');
var monk = require('monk');
var mongodb = require('mongodb');
var MongoClient = require('mongodb').MongoClient, test = require('assert');
var ObjectID = require('mongodb').ObjectID;
var db = monk('localhost:27017/nodetest1');

var loggedin =  false;
var loggedinas = "none";

var index = require('./routes/index');
var api = require('./routes/api');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('loggedin', loggedin);

//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

var collection = db.get('barcollection');
var id;
function find(){
  var collection = db.get('barcollection');
  collection.find({"name" : "placeholder"},{},function(e,docs){
    try{id=ObjectID(docs[0]._id);
        if(id != null){
          save(false);
        }else{
          save(true);
        }}
    catch(err){
    save(true);}
  });
};
function save(kappas){
  var collection = db.collection("barcollection");
  if(kappas){
    collection.insert({
        "name" : "placeholder",
        "locx" : "60.192059",
        "locy": "24.945831",
        "products" : {
               "Kalja1": {"name":"asd", "price":4.5},
               "Kalja2": {"name": "sdf", "price": 3.0},
               "Salmari": {"name": "qwe", "price": 2.0}
        },
        "type" : "pub",
        "capacity": "60"
    }, function (err, doc) {
        if (err) {
            res.send("There was a problem adding the information to the database.");
        }
        else {}
 });
};
};

var kappas = find();

// Make our db accessible to our router
app.use(function(req,res,next){
  req.db = db;
  req.loggedin = loggedin;
  req.MongoClient = MongoClient;
    next();
});

app.use('/', index);
app.use('/API', api);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
