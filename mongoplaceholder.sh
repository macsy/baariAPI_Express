#!/bin/bash
JAR=~/.m2/repository/pentaho/mondrian-data-foodmart-json/0.2/mondrian-data-foodmart-json-0.2.jar
mkdir /tmp/json
cd /tmp/json
jar xvf $JAR
for i in *.json; do
  mongoimport --db foodmart --collection ${i/.json/} --file $i
done
